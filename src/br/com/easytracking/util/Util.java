package br.com.easytracking.util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Util {

	public static final String PATH ="C:\\Maxtrack\\Maxtrack_Gateway\\xml\\data";
	public static final String PATH_COPY ="C:\\Maxtrack\\Maxtrack_Gateway\\xml\\arquivos_nao_enviados";
	public static final String PATH_COMMANDS ="C:\\Maxtrack\\Maxtrack_Gateway\\xml\\commands";
	public static final String PATH_COMMANDS_RESPONSE ="C:\\Maxtrack\\Maxtrack_Gateway\\xml\\commands_response";
	public static final String PATH_KEYSTORE ="/Users/biharck/Desktop";
	
	
	public static int qtdFiles(){
		File diretorio = new File(Util.PATH_COMMANDS);
		File fList[] = diretorio.listFiles();
		return fList.length;
	}
	
	public static String getCommandTimeout(){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Calendar cal = Calendar.getInstance();  
		cal.add(Calendar.DATE, 1);  
		return format.format(cal.getTime());
	}
}
