package br.com.easytracking.services;

import br.com.easytracking.tracker.commands.CortarCombustivelCommand;
import br.com.easytracking.tracker.commands.DesativarLimiteVelocidadeCommand;
import br.com.easytracking.tracker.commands.DesativarPanicoCommand;
import br.com.easytracking.tracker.commands.EnvioCoordenadaCommand;
import br.com.easytracking.tracker.commands.LiberarCombustivelCommand;
import br.com.easytracking.tracker.commands.LimiteVelocidadeCommand;
import br.com.easytracking.util.TokenUtil;

public class ReceiveCommands {
	
	/**
	 * 
	 * @param id
	 * @param commandType
	 * 		1 - corte
	 * 		2 - liberar
	 * 		3 - ativar limite velocidade
	 * 		4 - desativar limite velocidade
	 * 		5 - desativar p�nico
	 * 		6 - solicitar coordenada
	 * 		7 - sair do modo sleep
	 */
	public boolean commands(String id, String limiteVelocidade, int commandType, String token){
		if(!token.equals(TokenUtil.getToken()))
			return false;
		if(id==null)
			return false;
					
			switch (commandType) {
			case 1:
				 return CortarCombustivelCommand.generateXMLFile(id);
			case 2:
				return LiberarCombustivelCommand.generateXMLFile(id);
			case 3:
				return LimiteVelocidadeCommand.generateXMLFile(id, limiteVelocidade);
			case 4:
				return DesativarLimiteVelocidadeCommand.generateXMLFile(id);
			case 5:
				return DesativarPanicoCommand.generateXMLFile(id);
			case 6:
				return EnvioCoordenadaCommand.generateXMLFile(id);
			default:
				 return false;
			}
	}

}
