package br.com.easytracking.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/**
 * <p>Classe respons�vel por realizar a criptografia no padr�o 3DES segundo normativas FEBRABAN</p>
 * @author Biharck
 *
 */
public class CryptographyTripleDES {
	
	private Cipher cipher;
	private byte[] encryptKey;
	private KeySpec keySpec;
	private SecretKeyFactory secretKeyFactory;
	private SecretKey secretKey;
	
	
	public static CryptographyTripleDES newInstance()
		throws InvalidKeyException, UnsupportedEncodingException,NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException{
		return new CryptographyTripleDES();
	}
	
	private CryptographyTripleDES() throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException{
		String key = "47b4a0b915d1943c2ef70d825c176c80be446eca208bc742112f01c01a0d88b106368682b56b97a27979f204fc321b783792f37ee7667e96d3307f163e801089a1b3756b4d84d7d7eaf93375dfb11ec074fdfe57532237ac447a04915cab13fdafb69a223ffe005596afa36f7d2d112c7fb02c705d0efd9608985fa287f96bc4";
		encryptKey = key.getBytes( "UTF-8" );
		cipher = Cipher.getInstance( "DESede" );
		keySpec = new DESedeKeySpec( encryptKey );
		secretKeyFactory = SecretKeyFactory.getInstance( "DESede" );
		secretKey = secretKeyFactory.generateSecret( keySpec );
	}
	
}
