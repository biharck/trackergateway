package br.com.easytracking.encrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

/**
 * <p>Classe respons�vel pela valida��o/gera��o das criptografias do webservice na web</p>
 * 
 * @author Biharck
 *
 */
public class EncryptWeb {

	
	
	
	/** 
	 * <p>Retorna a assinatura para o buffer de bytes, usando a chave privada.</p> 
	 * @param key PrivateKey 
	 * @param buffer Array de bytes a ser assinado. 
	 * @return {@link Byte}[]
	 */
	public static byte[] createSignature( PrivateKey key, byte[] buffer ) throws Exception {  
	    Signature sig = Signature.getInstance("SHA1withRSA");  
	    sig.initSign(key);
	    sig.update(buffer, 0, buffer.length);  
	    return sig.sign();  
	} 
	
	/**
	 * <p>Retorna a chave privada de um arquivo<p>
	 * @param cert certificado digital
	 * @param alias alias do certificado
	 * @param password senha do certificado
	 * @return {@link PrivateKey}
	 * @throws Exception
	 */
	public static PrivateKey getPrivateKeyFromFile( File cert, String alias, String password ) throws Exception {
		KeyStore ks = KeyStore.getInstance("JKS");
		char[] pwd = password.toCharArray();
		InputStream is = new FileInputStream( cert );
		ks.load( is, pwd );
		is.close();
		Key key = ks.getKey( alias, pwd );
		if( key instanceof PrivateKey ) {
			return (PrivateKey) key;
		}
		return null;
	}

	/**
	 * <p>M�todo respons�vel em extrair a chave p�blica de um arquivo</p>
	 * @param cert certificado digital
	 * @param alias alias do certificado
	 * @param password senha do certificado
	 * @return {@link PublicKey}
	 * @throws Exception
	 */
	public static PublicKey getPublicKeyFromFile( File cert, String alias, String password ) throws Exception {
		KeyStore ks = KeyStore.getInstance ("JKS");
		char[] pwd = password.toCharArray();
		InputStream is = new FileInputStream( cert );
		ks.load( is, pwd );
		Key key = ks.getKey( alias, pwd );
		java.security.cert.Certificate c = ks.getCertificate( alias );
		PublicKey p = c.getPublicKey();
		return p;
	}

	/**
	 * <p> M�todo que verifica a legibilidade de uma assinatura com seu dado</p>
	 * @param key {@link PublicKey}
	 * @param buffer dado original
	 * @param signed assinatura deste dado
	 * @return {@link Boolean} true caso ok e false caso contr�rio
	 * @throws Exception
	 */
	public static boolean verifySignature( PublicKey key, byte[] buffer, String signed ) throws Exception {

		String[] certificadoSplit = signed.split("###");
		byte[] signedByteArray = new byte[certificadoSplit.length];
		for (int i = 0; i < certificadoSplit.length; i++) {
			if(certificadoSplit[i]==null || certificadoSplit[i].isEmpty())
				continue;
			signedByteArray[i] = new Byte(certificadoSplit[i]).byteValue();
		}
		
	    Signature sig = Signature.getInstance("SHA1withRSA");  
	    sig.initVerify(key);  
	    sig.update(buffer, 0, buffer.length);  
	    return sig.verify( signedByteArray );  
	}
}
