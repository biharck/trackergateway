package br.com.easytracking.tracker.xml;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis2.AxisFault;

import br.com.easytracking.tracker.bean.xsd.Coordenadas;
import br.com.easytracking.tracker.position.ReceiverPositionLocator;
import br.com.easytracking.util.TokenUtil;

public class SendXmlData {
	
	public static boolean sendPosition(Coordenadas[] coordenadas){
		try {
			ReceiverPositionLocator locator = new ReceiverPositionLocator();
			locator.getReceiverPositionHttpSoap11Endpoint().receiver(coordenadas,TokenUtil.getToken());
		} catch (AxisFault e) {
			e.printStackTrace();
			return false;
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		} catch (ServiceException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	public static void main(String[] args) {
		Coordenadas c = new Coordenadas();
		c.setSerial("123");
		
		Coordenadas[] cs = {c};
		
		sendPosition(cs);
	}

}
