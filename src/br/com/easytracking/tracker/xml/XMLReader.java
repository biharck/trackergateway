package br.com.easytracking.tracker.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.com.easytracking.tracker.bean.xsd.Coordenadas;
import br.com.easytracking.util.Util;

public class XMLReader extends TimerTask {
	

	public void run() {
		XMLReader reader = new XMLReader();
		reader.sendData();
    }
	
	public boolean sendData(){
		
		File diretorio = new File(Util.PATH);
		File fList[] = diretorio.listFiles();

		
		
		if(fList==null) return true;

		int tam = (fList.length > 100) ? 100 : fList.length;
			
		System.out.println("Numero de arquivos no diretorio : " + fList.length );
		System.out.println("Numero de arquivos a serem processados : " + tam );

		for ( int i = 0; i < tam; i++ ){
			System.out.println("Arquivo :" + fList[i].getName());
			boolean envioOk = enviarCoordenadas(fList[i]);
			if(!envioOk) {
				try {
					System.out.println("Arquivo n�o enviado");
					copyFile(fList[i]);
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Movido para pasta de arquivos n�o enviados! ");
			}
		}
		
		for ( int i = 0; i < tam; i++ ){
			if(fList[i].delete()){
    			System.out.println(fList[i].getName() + " foi apagado!");
    		}else{
    			System.out.println("A opera��o de apagar o registro foi abortada.");
    		}
		}
		System.out.println("----------------------------");
		return true;
	}
	
	
	private boolean enviarCoordenadas(File file) {
		
		List<Coordenadas> coordenadas = new ArrayList<Coordenadas>();
		
		try {
			 
			File fXmlFile = file;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
//			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		 
			NodeList nList = doc.getElementsByTagName("POSITION");
		 
			for (int temp = 0; temp < nList.getLength(); temp++) {
		 
				Node nNode = nList.item(temp);
//				System.out.println("\nCurrent Element :" + nNode.getNodeName());
		 
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
					
					String serial = eElement.getElementsByTagName("SERIAL").item(0).getTextContent();
					String transmissionReason = eElement.getElementsByTagName("TRANSMISSION_REASON").item(0).getTextContent();
					String date = eElement.getElementsByTagName("DATE").item(0).getTextContent();
					String latitude = eElement.getElementsByTagName("LATITUDE").item(0).getTextContent();
					String longitude = eElement.getElementsByTagName("LONGITUDE").item(0).getTextContent();
					String course = eElement.getElementsByTagName("COURSE").item(0).getTextContent();
					String speed = eElement.getElementsByTagName("SPEED").item(0).getTextContent();
					String csq = eElement.getElementsByTagName("CSQ").item(0).getTextContent();
					String svn = eElement.getElementsByTagName("SVN").item(0).getTextContent();
					String snr = eElement.getElementsByTagName("SNR").item(0).getTextContent();
					String gprsConnection = eElement.getElementsByTagName("GPRS_CONNECTION").item(0).getTextContent();
					String gpsSignal = eElement.getElementsByTagName("GPS_SIGNAL").item(0).getTextContent();
					String gpsAntennaFailure = eElement.getElementsByTagName("GPS_ANTENNA_FAILURE").item(0).getTextContent();
					String gpsAntennaDisconnected = eElement.getElementsByTagName("GPS_ANTENNA_DISCONNECTED").item(0).getTextContent();
					String excessSpeed = eElement.getElementsByTagName("EXCESS_SPEED").item(0).getTextContent();
					String gpsSleep = eElement.getElementsByTagName("GPS_SLEEP").item(0).getTextContent();
					String gsmJamming = eElement.getElementsByTagName("GSM_JAMMING").item(0).getTextContent();
					String moving = eElement.getElementsByTagName("MOVING").item(0).getTextContent();
					String temperature = eElement.getElementsByTagName("TEMPERATURE").item(0).getTextContent();
					String input1 = eElement.getElementsByTagName("INPUT1").item(0).getTextContent();
					String panic = eElement.getElementsByTagName("PANIC").item(0).getTextContent();
					String output1 = eElement.getElementsByTagName("OUTPUT1").item(0).getTextContent();
					String antiTheftStatus = eElement.getElementsByTagName("ANTI_THEFT_STATUS").item(0).getTextContent();
					String batteryCharging = eElement.getElementsByTagName("BATTERY_CHARGING").item(0).getTextContent();
					String batteryFailure = eElement.getElementsByTagName("BATTERY_FAILURE").item(0).getTextContent();
					
					
					Coordenadas coordenada = new Coordenadas();
					coordenada.setSerial(serial);
					coordenada.setTransmissionReason(transmissionReason);
					coordenada.setDate(date);
					coordenada.setLatitude(latitude);
					coordenada.setLongitude(longitude);
					coordenada.setCourse(course);
					coordenada.setSpeed(speed);
					coordenada.setCsq(csq);
					coordenada.setSvn(svn);
					coordenada.setSnr(snr);
					coordenada.setGprsConnection(gprsConnection);
					coordenada.setGpsSignal(gpsSignal);
					coordenada.setGpsAntennaFailure(gpsAntennaFailure);
					coordenada.setGpsAntennaDisconnected(gpsAntennaDisconnected);
					coordenada.setExcessSpeed(excessSpeed);
					coordenada.setGpsSleep(gpsSleep);
					coordenada.setGsmJamming(gsmJamming);
					coordenada.setMoving(moving);
					coordenada.setTemperature(temperature);
					coordenada.setPanic(panic);
					coordenada.setInput1(input1);
					coordenada.setOutput1(output1);
					coordenada.setAntiTheftStatus(antiTheftStatus);
					coordenada.setBatteryCharging(batteryCharging);
					coordenada.setBatteryFailure(batteryFailure);
					
					coordenadas.add(coordenada);
				}
			}
			Coordenadas[] coordenadasArray = new Coordenadas[coordenadas.size()];
			coordenadas.toArray(coordenadasArray);
			System.out.println("----------------------------");
			return SendXmlData.sendPosition(coordenadasArray);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return false;
	    }
	}
	
    private void copyFile(File source) throws IOException {
    	File destination = new File(Util.PATH_COPY + "\\" + source.getName());
    	
        if (destination.exists())
            destination.delete();

        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;

        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen())
                sourceChannel.close();
            if (destinationChannel != null && destinationChannel.isOpen())
                destinationChannel.close();
       }
   }

}
