package br.com.easytracking.tracker.commands;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import br.com.easytracking.util.Util;

public class DesativarLimiteVelocidadeCommand {

	public static boolean generateXMLFile(String serial_id) {
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			Document commands = docBuilder.newDocument();
			Element rootElement = commands.createElement("COMMANDS");
			commands.appendChild(rootElement);

			// COMMAND
			Element command = commands.createElement("COMMAND");
			rootElement.appendChild(command);

			Element protocol = commands.createElement("PROTOCOL");
			protocol.appendChild(commands.createTextNode("166"));
			command.appendChild(protocol);

			Element serial = commands.createElement("SERIAL");
			serial.appendChild(commands.createTextNode(serial_id));
			command.appendChild(serial);

			Element id_command = commands.createElement("ID_COMMAND");
			id_command.appendChild(commands.createTextNode("MAX_SPEED_LIMIT_"+(Math.random() * 2)));
			command.appendChild(id_command);

			Element type = commands.createElement("TYPE");
			type.appendChild(commands.createTextNode("51"));
			command.appendChild(type);

			Element attempts = commands.createElement("ATTEMPTS");
			attempts.appendChild(commands.createTextNode("5"));
			command.appendChild(attempts);

			Element commandTimeOut = commands.createElement("COMMAND_TIMEOUT");
			commandTimeOut.appendChild(commands.createTextNode(Util.getCommandTimeout()));
			command.appendChild(commandTimeOut);

			// COMMAND
			Element parameters = commands.createElement("PARAMETERS");
			command.appendChild(parameters);
			
			//PARAMETER
			Element parameter = commands.createElement("PARAMETER");
			parameters.appendChild(parameter);

			Element id = commands.createElement("ID");
			id.appendChild(commands.createTextNode("SET_MAX_SPEED_LIMIT"));
			parameter.appendChild(id);
			
			Element value = commands.createElement("VALUE");
			value.appendChild(commands.createTextNode("1"));
			parameter.appendChild(value);
			
			//PARAMETER 2
			Element parameter2 = commands.createElement("PARAMETER");
			parameters.appendChild(parameter2);

			Element id2 = commands.createElement("ID");
			id2.appendChild(commands.createTextNode("MAX SPEED LIMIT"));
			parameter2.appendChild(id2);
			
			Element value2 = commands.createElement("VALUE");
			value2.appendChild(commands.createTextNode("250"));
			parameter2.appendChild(value2);

			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			DOMSource source = new DOMSource(commands);
			String fileName = Util.PATH_COMMANDS+"\\C_166_"+serial_id+"_"+(Util.qtdFiles()+1)+".cmd";
			StreamResult result = new StreamResult(new File(fileName));

			// Output to console for testing
//			 StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("Arquivo "+ fileName + " Criado com sucesso!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
			return false;
		}
		return true;
	}
}
