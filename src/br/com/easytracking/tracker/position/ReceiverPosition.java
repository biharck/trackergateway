/**
 * ReceiverPosition.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.easytracking.tracker.position;

public interface ReceiverPosition extends javax.xml.rpc.Service {
    public java.lang.String getReceiverPositionHttpSoap11EndpointAddress();

    public br.com.easytracking.tracker.position.ReceiverPositionPortType getReceiverPositionHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public br.com.easytracking.tracker.position.ReceiverPositionPortType getReceiverPositionHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
