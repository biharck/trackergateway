/**
 * ReceiverPositionLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.easytracking.tracker.position;

public class ReceiverPositionLocator extends org.apache.axis.client.Service implements br.com.easytracking.tracker.position.ReceiverPosition {

    public ReceiverPositionLocator() {
    }


    public ReceiverPositionLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ReceiverPositionLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ReceiverPositionHttpSoap11Endpoint
    private java.lang.String ReceiverPositionHttpSoap11Endpoint_address = "http://www.trackhost.srv.br/TrackerServer/services/ReceiverPosition.ReceiverPositionHttpSoap11Endpoint/";

    public java.lang.String getReceiverPositionHttpSoap11EndpointAddress() {
        return ReceiverPositionHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ReceiverPositionHttpSoap11EndpointWSDDServiceName = "ReceiverPositionHttpSoap11Endpoint";

    public java.lang.String getReceiverPositionHttpSoap11EndpointWSDDServiceName() {
        return ReceiverPositionHttpSoap11EndpointWSDDServiceName;
    }

    public void setReceiverPositionHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        ReceiverPositionHttpSoap11EndpointWSDDServiceName = name;
    }

    public br.com.easytracking.tracker.position.ReceiverPositionPortType getReceiverPositionHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ReceiverPositionHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getReceiverPositionHttpSoap11Endpoint(endpoint);
    }

    public br.com.easytracking.tracker.position.ReceiverPositionPortType getReceiverPositionHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.easytracking.tracker.position.ReceiverPositionSoap11BindingStub _stub = new br.com.easytracking.tracker.position.ReceiverPositionSoap11BindingStub(portAddress, this);
            _stub.setPortName(getReceiverPositionHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setReceiverPositionHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        ReceiverPositionHttpSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.easytracking.tracker.position.ReceiverPositionPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.easytracking.tracker.position.ReceiverPositionSoap11BindingStub _stub = new br.com.easytracking.tracker.position.ReceiverPositionSoap11BindingStub(new java.net.URL(ReceiverPositionHttpSoap11Endpoint_address), this);
                _stub.setPortName(getReceiverPositionHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ReceiverPositionHttpSoap11Endpoint".equals(inputPortName)) {
            return getReceiverPositionHttpSoap11Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://position.tracker.easytracking.com.br", "ReceiverPosition");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://position.tracker.easytracking.com.br", "ReceiverPositionHttpSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ReceiverPositionHttpSoap11Endpoint".equals(portName)) {
            setReceiverPositionHttpSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
