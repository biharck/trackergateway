/**
 * ReceiverPositionPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.easytracking.tracker.position;

public interface ReceiverPositionPortType extends java.rmi.Remote {
    public void receiver(br.com.easytracking.tracker.bean.xsd.Coordenadas[] coordenadas, java.lang.String token) throws java.rmi.RemoteException;
}
