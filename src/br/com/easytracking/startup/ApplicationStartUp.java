package br.com.easytracking.startup;

import java.awt.Toolkit;
import java.io.IOException;
import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.easytracking.tracker.xml.XMLReader;

public class ApplicationStartUp extends HttpServlet {
	
	Toolkit toolkit;
    Timer timer;
    
	public void init() throws ServletException {
		// / Automatically java script can run here
		System.out.println("************************************************");
		System.out.println("*** Servlet Initialized successfully ***..");
		System.out.println("************************************************");

		System.out.println("Iniciando Thread...");
		toolkit = Toolkit.getDefaultToolkit();
        timer = new Timer();
        timer.schedule(new XMLReader(),
                       0,        //initial delay
                       30*1000);  //subsequent rate
        
        System.out.println("Thread iniciada com sucesso!");

	}

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
